
def train_model(data, label, dictionary, map_data):
    import copy
    import pandas as pd
    import numpy as np

    dictionary_size = len(dictionary)
    estimates = [None] * dictionary_size

    priors, conditional_probabilities = [], []
    for idx, news_group in enumerate(map_data.values):

        BE, MLE = copy.copy(estimates), copy.copy(estimates)

        group_docs = label.loc[label["label"] == news_group[0]]
        prior = len(group_docs) / len(label)
        priors.append(prior)

        class_doc_indices = group_docs.index + 1
        class_words = data.loc[data["doc_id"].isin(class_doc_indices)]
        N = class_words["count"].sum()  # number of words in all documents in a given class

        for word, word_idx in dictionary.items():

            idx = word_idx - 1
            word_occurrences = class_words.loc[class_words["word_id"] == word_idx]

            if word_occurrences.empty:

                mle_estimate = 0 / (N + 1)  # TODO: this might be zero
                MLE[idx] = mle_estimate

                bayesian_estimate = (0 + 1) / (N + dictionary_size)
                BE[idx] = bayesian_estimate
            else:
                n_k = word_occurrences["count"].sum()

                mle_estimate = n_k / N
                MLE[idx] = mle_estimate

                bayesian_estimate = (n_k + 1) / (N + dictionary_size)
                BE[idx] = bayesian_estimate

        conditional_probabilities.append((MLE, BE))

    [print("P(Omega=%i):%.4f" % (i + 1, prior)) for i, prior in enumerate(priors)]

    result = pd.DataFrame()
    for group in range(len(conditional_probabilities)):
        column_name1 = "MLE_" + str(group+1)
        column_name2 = "BE_" + str(group+1)
        result[column_name1] = pd.Series(np.asarray(conditional_probabilities[group][0]))
        result[column_name2] = pd.Series(np.asarray(conditional_probabilities[group][1]))

    output_file = download_file("conditional_probabilities.pkl")
    result.to_pickle(output_file)

    result = pd.DataFrame()
    column_name = "class_prior"
    result[column_name] = pd.Series(np.asarray(priors))

    output_file = download_file("prior_probabilities.pkl")
    result.to_pickle(output_file)

    return priors, conditional_probabilities
    # return None, None


def download_file(file_name):
    import os
    cur_path = os.path.abspath(os.path.dirname(__file__))
    full_file_path = cur_path + "/" + file_name
    return full_file_path
