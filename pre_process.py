
def data_uploads():
    import numpy as np
    import pandas as pd
    import os

    file_folder = "/data/"  # "\\data\\"  # TODO: windows and linux versions
    cur_path = os.path.abspath(os.path.dirname(__file__))

    train_data_file = "train_data.csv"
    full_path = cur_path + file_folder + train_data_file
    columns = ["doc_id", "word_id", "count"]
    data_types = [(columns[0], np.uint64), (columns[1], np.uint64), (columns[2], np.uint64)]
    train_data = pd.read_csv(full_path, header=None, dtype=data_types)
    train_data.columns = columns

    train_label_file = "train_label.csv"
    full_path = cur_path + file_folder + train_label_file
    columns = ["label"]
    data_types = [(columns[0], np.uint64)]
    train_label = pd.read_csv(full_path, header=None, dtype=data_types)
    train_label.columns = columns

    test_data_file = "test_data.csv"
    full_path = cur_path + file_folder + test_data_file
    columns = ["doc_id", "word_id", "count"]
    data_types = [(columns[0], np.uint64), (columns[1], np.uint64), (columns[2], np.uint64)]
    test_data = pd.read_csv(full_path, header=None, dtype=data_types, index_col=False)
    test_data.columns = columns

    test_label_file = "test_label.csv"
    full_path = cur_path + file_folder + test_label_file
    columns = ["label"]
    data_types = [(columns[0], np.uint64)]
    test_label = pd.read_csv(full_path, header=None, dtype=data_types, index_col=False)
    test_label.columns = columns

    map_file = "map.csv"
    full_path = cur_path + file_folder + map_file
    columns = ["group_id", "group_name"]
    data_types = [(columns[0], np.uint64), (columns[1], type(""))]
    map_data = pd.read_csv(full_path, header=None, dtype=data_types, index_col=None)

    vocabulary_file = "vocabulary.txt"
    full_path = cur_path + file_folder + vocabulary_file
    opened_file, dictionary = open(full_path, "r"), {}
    for idx, word in enumerate(opened_file):
        dictionary[word] = idx+1

    return train_data, train_label, test_data, test_label, map_data, dictionary
