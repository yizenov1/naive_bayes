
Python implementation of Naive Bayes Classifier
Dataset: "The 20 Newsgroups"

used libraries:
1. numpy (vectors)
2. pandas (table)
3. tabulate (visualization)

put input files into folder "data" in the same directory where the pythons files are located

provide input parameter for the main function

1. train BE
2. test BE
3. test MLE


intermediate outputs are saved for convenience

1. prior_probabilities.pkl
2. conditional_probabilities.pkl

3. result_train_BE.pkl
4. result_test_BE.pkl
5. result_test_MLE.pkl


approximate speed of computations

learning: Training stage: (hh:mm:ss.ms) 0:06:51.076911
prediction (train BE): Testing stage: (hh:mm:ss.ms) 0:00:58.575324
prediction (test BE): Testing stage: (hh:mm:ss.ms) 0:00:35.195076
prediction (test MLE): Testing stage: (hh:mm:ss.ms) 0:00:37.231979