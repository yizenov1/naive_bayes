
import pre_process
import model


def upload_file(file_name):
    import os
    cur_path = os.path.abspath(os.path.dirname(__file__))
    full_file_path = cur_path + "/" + file_name
    return full_file_path


def predict1(words, priors, conditional_probabilities, estimate_type):
    class_predictions = []

    for idx in range(len(priors)):

        column_name = estimate_type + "_" + str(idx + 1)
        word_class_probabilities = conditional_probabilities[column_name]

        adj_words = words.values - 1
        probabilities = word_class_probabilities.iloc[adj_words]

        conditional_product = 1
        for prob in probabilities:
            conditional_product *= prob

        prior_probability = priors.iloc[idx].values[0]
        class_predictions.append(prior_probability * conditional_product)

    max_probability = max(class_predictions)
    index = class_predictions.index(max_probability) + 1
    return index


def predict2(words, priors, conditional_probabilities, estimate_type):
    import numpy as np
    class_predictions = []

    for idx in range(len(priors)):

        column_name = estimate_type + "_" + str(idx+1)
        word_class_probabilities = conditional_probabilities[column_name]

        adj_words = words.values - 1
        probabilities = word_class_probabilities.iloc[adj_words]

        conditional_sum = sum(np.log(probabilities))  # TODO: log might receive zeroes from MLE

        prior_probability = priors.iloc[idx].values[0]
        probability = np.log(prior_probability) + conditional_sum
        class_predictions.append(probability)

    max_probability = max(class_predictions)
    index = class_predictions.index(max_probability) + 1
    return index


def overall_accuracy(results):

    correct_predictions = 0
    for i in range(len(results)):
        result = results.iloc[i]
        if result["prediction"] == result["label"]:
            correct_predictions += 1

    accuracy = correct_predictions / len(results)
    return accuracy


def class_accuracy(results, groups):

    for group in range(1, len(groups)+1):

        class_documents = results.loc[results["label"] == group].index
        documents = results.iloc[class_documents]

        overall_class = overall_accuracy(documents)
        print("Group %i: %.4f" % (group, overall_class))


def confusion_matrix(results, true_labels, groups):
    # from tabulate import tabulate
    rows = []
    for i in range(1, len(groups) + 1):
        row = [0] * len(groups)
        class_docs = true_labels.loc[true_labels["label"] == i].index
        documents = results.iloc[class_docs]
        for j in range(len(documents)):
            prediction = documents.iloc[j]["prediction"]
            row[int(prediction-1)] += 1
        rows.append(row)
        print(row, end='')
        print("")
    # print(tabulate(rows, tablefmt='orgtbl'))


def start_model(inn):
    import pandas as pd
    import numpy as np
    import copy
    from datetime import datetime

    start_time = datetime.now()

    train_data, train_label, test_data, test_label, map_data, dictionary = pre_process.data_uploads()
    priors, conditional_probabilities = model.train_model(train_data, train_label, dictionary, map_data)

    time_elapsed = datetime.now() - start_time
    print("Training stage: (hh:mm:ss.ms) {}".format(time_elapsed))

    start_time = datetime.now()

    """input_file = upload_file("conditional_probabilities.pkl")
    conditional_probabilities = pd.read_pickle(input_file)

    input_file = upload_file("prior_probabilities.pkl")
    priors = pd.read_pickle(input_file)"""

    if inn == 1:
        print("Train data on Bayesian estimates")
        document_ids = train_label.index.values

        predictions = np.zeros(len(document_ids)+1, dtype=np.uint64)
        for doc_id in range(len(document_ids)):
            words = train_data.loc[train_data["doc_id"] == doc_id+1]["word_id"]
            predicted_class = predict2(words, priors, conditional_probabilities, estimate_type="BE")  # Bayesian
            predictions[doc_id+1] = predicted_class

        results = copy.copy(train_label)
        results["prediction"] = pd.Series(predictions[1:len(predictions)])

        """output_file = upload_file("result_train_BE.pkl")
        train_label.to_pickle(output_file)
    
        input_file = upload_file("result_train_BE.pkl")
        results = pd.read_pickle(input_file)"""

        overall = overall_accuracy(results)
        print("Overall Accuracy = %.4f" % overall)

        print("\nClass accuracy:")
        class_accuracy(results, map_data)

        print("\nConfusion matrix:")
        confusion_matrix(results, train_label, map_data)
    ###################################################################################################################
    elif inn == 2:
        print("Test data on Bayesian estimates")
        document_ids = test_label.index.values

        predictions = np.zeros(len(document_ids)+1, dtype=np.uint64)
        for doc_id in range(len(document_ids)):
            words = test_data.loc[test_data["doc_id"] == doc_id + 1]["word_id"]
            predicted_class = predict2(words, priors, conditional_probabilities, estimate_type="BE")  # Bayesian
            predictions[doc_id+1] = predicted_class

        results = copy.copy(test_label)
        results["prediction"] = pd.Series(predictions)

        """output_file = upload_file("result_test_BE.pkl")
        test_label.to_pickle(output_file)
    
        input_file = upload_file("result_test_BE.pkl")
        results = pd.read_pickle(input_file)"""

        overall = overall_accuracy(results)
        print("Overall Accuracy = %.4f" % overall)

        print("\nClass accuracy:")
        class_accuracy(results, map_data)

        print("\nConfusion matrix:")
        confusion_matrix(results, test_label, map_data)
    ###################################################################################################################
    elif inn == 3:
        print("Test data on MLE")
        document_ids = test_label.index.values

        predictions = np.zeros(len(document_ids)+1, dtype=np.uint64)
        for doc_id in range(len(document_ids)):
            words = test_data.loc[test_data["doc_id"] == doc_id + 1]["word_id"]
            predicted_class = predict2(words, priors, conditional_probabilities, estimate_type="MLE")  # MLE
            predictions[doc_id+1] = predicted_class

        results = copy.copy(test_label)
        results["prediction"] = pd.Series(predictions)

        """output_file = upload_file("result_test_MLE.pkl")
        test_label.to_pickle(output_file)

        input_file = upload_file("result_test_MLE.pkl")
        results = pd.read_pickle(input_file)"""

        overall = overall_accuracy(results)
        print("Overall Accuracy = %.4f" % overall)

        print("\nClass accuracy:")
        class_accuracy(results, map_data)

        print("\nConfusion matrix:")
        confusion_matrix(results, test_label, map_data)

    time_elapsed = datetime.now() - start_time
    print("Testing stage: (hh:mm:ss.ms) {}".format(time_elapsed))

# TODO: create executable file
start_model(inn=1)
